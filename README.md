<div>
<img width="100%" height="93" src="https://screen-play.app/images/logoSP.svg">
</div>

<div align="center">
Documentation about the ScreenPlay website, forum and organisation setup.
<br>
</div>
<br>


## ScreenPlay Documentation

 * Hosted https://kelteseth.gitlab.io/ScreenPlayDocs/
 * Gitlab https://gitlab.com/kelteseth/ScreenPlayDocs/

## Website

 * Hosted https://screen-play.app/ 
 * Gitlab https://gitlab.com/kelteseth/screen-play.app

## NodeBB Forum Setup
 * Hosted https://forum.screen-play.app/ 

 1. Install Ubuntu LTS minimal docker

#### NGINX

 1. [Install nginx with SSL](https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-18-04)
    1. Add cerbot repo to ubuntu
        * ```add-apt-repository ppa:certbot/certbot```
    2. Install nginx cerbot & fail2ban
        *  ```apt install nginx python-certbot-nginx fail2ban```
    3. Generate cert
        * ```certbot --nginx -d forum.screen-play.app```
        * Disallow enable https redirect during the cerbot setup. 
  2. Setup ssl and reserve proxy via nginx
     * ```vim /etc/nginx/sites-available/forum.screen-play.app ```
     * Add the following snippted [(based of nodebb docs)](https://docs.nodebb.org/configuring/proxies/nginx/#basic-with-ssl): 

```

upstream nodebb {
    server 127.0.0.1:4567;
}

server {
    listen 80;
    server_name forum.screen-play.app;
    return 302 https://$server_name$request_uri;

}

# server configuration

server {

        listen 443 ssl;
        server_name forum.screen-play.app;
         # enables all versions of TLS, but not SSLv2 or 3 which are weak and now deprecated.
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;

        # disables all weak ciphers
        ssl_ciphers 'AES128+EECDH:AES128+EDH';

        ssl_prefer_server_ciphers on;
        #access_log /var/log/nginx/nodebb.access.log;
        #error_log /var/log/nginx/nodebb.error.log;

        location / {

                # File upload
                # https://community.nodebb.org/topic/12815/error-request-entity-too-large/3
                client_max_body_size 50M;

                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Proto $scheme;
                proxy_set_header Host $http_host;
                proxy_set_header X-NginX-Proxy true;
                proxy_pass http://127.0.0.1:4567;
                proxy_redirect off;

                # Socket.IO Support
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";

        }

    # generated by certbot
    #listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/forum.screen-play.app/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/forum.screen-play.app/privkey.pem; # managed by Certbot
    #include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    #ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}
```

 3. Enable config via symlink
    * ``` ln -s /etc/nginx/sites-available/forum.screen-play.app /etc/nginx/sites-enabled/forum.screen-play.app ```
 4. Restart nginx
    * ``` service nginx restart ```

#### NodeBB
 1. [Create nodebb via docker container](https://hub.docker.com/r/nilsramsperger/nodebb/)
    * ```docker create --name ScreenPlayNodeBB --init --restart always -p 4567:4567 -v nodebb-data:/var/lib/redis -v nodebb-files:/opt/nodebb/public/uploads -v nodebb-config:/etc/nodebb nilsramsperger/nodebb:latest```
 2.  ```docker start ScreenPlayNodeBB ```
 3. Setup installation via forum.screen-play.app
    * Set default database empty and click setup
 4. When you only see the default nginx page the remove all `# managed by Certbot` from your  `/etc/nginx/sites-available/default` config that correspond to ScreenPlay because these entries would be exciting twice causing nginx to do nothing.


##### Update Nodebb
Because all data is saved outside we can simply remove the old container and create a new one with the same name from the latest docker by nilsramsperger/nodebb. 
1.  ```docker stop ScreenPlayNodeBB ```
2.  ```docker pull  nilsramsperger/nodebb```
3.  ```docker rm ScreenPlayNodeBB```
4.  ```docker create --name ScreenPlayNodeBB --init --restart always -p 4567:4567 -e url=https://forum.screen-play.app/ -v nodebb-data:/var/lib/redis -v nodebb-files:/opt/nodebb/public/uploads -v nodebb-config:/etc/nodebb nilsramsperger/nodebb:latest```
4.  ```docker start ScreenPlayNodeBB ```

##### (Optional) Enter Docker container
1.  ```docker ps ```
2.  ```docker exec -it [container-id] bash```
